
Get Server Version (aka Tango version) for all hosts:

code::

    $ for d in $(fandango find_devices tango/admin/*); do echo "$d $(fandango get_server_version $d)"; done
    tango/admin/cdrf4 
    tango/admin/crfws01 4
    tango/admin/crfws02 4
    tango/admin/ctrflab03 
    tango/admin/ctrflab05 3
    tango/admin/pc183 
    tango/admin/pc209 
    tango/admin/pc228 
    tango/admin/pc368 5
    tango/admin/pc370 5
    tango/admin/tlarf01 5
